const express = require('express')
const multer = require('multer')
const Tesseract = require('tesseract.js')
const dotenv = require('dotenv')

dotenv.config()

// Models
const Text = require('./models/Text')

// DB Connection
const connectMongoDB = require('./config/mongoDb')

connectMongoDB()

const app = express()

const port = process.env.PORT || 5001

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './uploads/')
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
})

const upload = multer({ storage: storage })

app.post('/api/upload', upload.single('image'), async (req, res) => {
    console.log(req.file)

    try {
        
        const { data: {text} } = await Tesseract.recognize(
            `uploads/${req.file.filename}`,
            'eng',
            {
                logger: m => console.log(m)
            }
        )

        // Save extracted text to database
        const document = await Text.create({
            text: text,
            fileName: req.file.filename
        })

        return res.json({
            message: text
        })

    } catch (error) {
        console.error(error)
    }

})

app.get('/api/documents', async (req, res) => {

    try {
     
        const documents = await Text.find()

        res.json(documents)

    } catch (error) {
        console.error(error)
    }
    
})

app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
})