# Node.js Optical Character Recognition API

A simple API with OCR capabilities. 

This simple application will extract text from images and save it to a database.

## Technologies

- Node.js
- Express.js
- Tesseract.js
- MongoDB

## Setup Instructions

- Clone the repository to your local environment

- Run `npm install` to install all project dependencies

- Copy the contents of the `.env.example` file into a `.env` file and add your MongoDB URI for database connectivity

## Usage

- Run `npm run dev` to start the node dev server with `nodemon` or `npm start` to start the node server

- Open any API client of your choice such as Postman and test the endpoints.