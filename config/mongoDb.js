const mongoose = require('mongoose')

const connectDB = async () => {
    const mongoURI = process.env.MONGO_URI

    const conn = await mongoose.connect(mongoURI, {})

    console.log(`Mongo DB Connected: ${conn.connection.host}`)
}

module.exports = connectDB